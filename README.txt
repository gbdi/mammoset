===================================================================
[2020/10/26]
We removed the BI-RADS fields of DDSM entries in the dataset.
Please refer to the official documentation for further information:
http://www.eng.usf.edu/cvprg/Mammography/DDSM/ddsm_terminology.html
===================================================================

MAMMOSET is available for researchers and data scientists under the Creative Commons BY license.
In case of publication and/or public use of MAMMOSET, as well as any dataset derived from it, one should acknowledge its creators by citing the following paper.

@inproceedings{mammoset,
    author    = {Oliveira, P. H. and
                 Scabora, L. C. and
                 Cazzolato, M. T. and
                 Bedo, M. V. N. and
                 Traina, A. J. M. and
                 Traina-Jr., C.},
    booktitle = {Proceedings of the Satellite Events of the 32nd Brazilian Symposium on Databases},
    title     = {{MAMMOSET: An Enhanced Dataset of Mammograms}},
    year      = {2017},
    pages     = {256--266},
    publisher = {{SBC}}
}
